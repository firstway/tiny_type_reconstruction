# -*- coding: utf8 -*-










_COUNT = 0

def newtvar():
    global _COUNT
    _COUNT = _COUNT+ 1
    return '_a'+str(_COUNT)


'''
(define init_E  ;; Type Environment 
  '((add1 (int -> int)) (sub1 (int -> int))
    (zero? (int -> bool)) (not (bool -> bool))
    (and (bool -> (bool -> bool))) (or (bool -> (bool -> bool)))))
'''
_E = {
        'add1':     ['int', '->', 'int'],
        'sub1':     ['int', '->', 'int'],
        'zero?':    ['int', '->', 'bool'],
        'not':      ['bool', '->', 'bool'],
        'and':      ['bool', '->', ['bool', '->', 'bool' ] ],
        'or':       ['bool', '->', ['bool', '->', 'bool' ] ],
    }

_C =  []

def error(s):
    raise Exception(s)

def get_in_Env(x):
    if x in _E:
        return _E[x]
    else:
        return None

def put_in_Env(k, expr):
    _E[k] = expr


def nonvariable(a):
    if a in ('int', 'bool'):
        return True
    return False

def get_in_constraint(x, C):
    for item in C:
        if x in item:
            return item[-1]
    return None

def put_in_constraint(k, v, C):
    i = 0
    for item in C:
        if k in item:
            break
        i += 1
    j=0
    for item in C:
        if v in item:
            break
        j += 1
    if i >= len(C) and i==j:
        C.append([k,v])
        return C
    elif i == j:
        return C
    elif i >= len(C):#add k
        equivalence = C[j]
        if nonvariable(k):
            equivalence.append(k)
        else:
            equivalence.insert(0,k)
        return C
    elif j >= len(C):#add v
        equivalence = C[i]
        if nonvariable(v):
            equivalence.append(v)
        else:
            equivalence.insert(0,v)
        return C
    elif i < len(C) and j < len(C):
        error("need merge")
    else:
        error("never happen")



def myunify(x, y, C):
    if x == y:
        return True
    if x == 'int' and y == 'bool':
        return False
    if x == 'bool' and y == 'int':
        return False
    if x in ('int', 'bool'):
        yc = get_in_constraint(y, C)
        if nonvariable(yc) and yc != x:
            error("type conflit: %s vs %s" % (yc, x)) 
        else:
            put_in_constraint(y, x, C)
            return True ##?
    if y in ('int', 'bool'):
        return myunify(y, x, C)
    #two are free vaule????
    put_in_constraint(y, x, C)
    return True


def gen_ast2():
    '''
    (&apply (&lambda (x) (&apply (&var sub1) (&var x))) (&const 5))
    '''
    head = 'x'
    fun=['&var', 'sub1']
    varx=['&var', 'x']
    body = ['&apply', fun, varx]
    lam = ['&lambda',head, body]
    constant_v = ['&const', 5]
    return ['&apply', lam, constant_v ]

_ast2 = gen_ast2()

def myinfer_lambda(x, bodyL, C):
    '''
    should return [a, -> , b]
    body has two:
        (var?)
        (e1 e2)
    eg: (&lambda (x) (&apply (&var add1) (&var x)))
    body_head = bodyL[0]
    if head == '&apply':

    elif head == '&lambda':
    elif head == '&const':
    elif head == '&var':
    else:
        error("incorrect head:"+str(L[0]))
    '''


def myinfer_in(L, C):
    head = L[0]
    #print "L->",L
    #print "C->",C
    if head == '&apply':
        '''
        E e1 : (α → β) E e2 : α
              E (e1 e2 ) : β
        '''
        e1,C1 = myinfer_in(L[1], C[:])
        e2,C2 = myinfer_in(L[2], C[:])
        if myunify(e1[0], e2, C):
            print L,"->infer->",e1[2]
            return e1[2],C
    elif head == '&lambda':
        '''
        E x : α E[x → α] e : β
        E (lambda( x ) e) : α → β
        '''
        a = newtvar()
        C1 = C[:]
        put_in_constraint( L[1], a, C1)
        e,C1 = myinfer_in(L[2], C1)#body
        a = get_in_constraint(a, C1) # myinfer may infer something for a
        print L,"->infer->",[a, '->', e]
        return [a, '->', e],C1
    elif head == '&const':
        if type(0) == type(L[1]):
            print L,"->infer->",'int'
            return 'int',C
        elif type(True) == type(L[1]):
            print L,"->infer->",'bool'
            return 'bool',C
        else:
            error("incorrect &const:"+str(L[1]))
    elif head == '&var':
        if get_in_Env(L[1]):
            print L,"->infer->",get_in_Env(L[1])
            return get_in_Env(L[1]),C
        elif get_in_constraint(L[1], C):
            print L,"->infer->",get_in_constraint(L[1], C)
            return get_in_constraint(L[1], C),C
        else:
            error("incorrect &var:"+str(L[1]))
    else:
        error("incorrect head:"+str(L[0]))


def myinfer(L, C):
    r, C = myinfer_in(L, C)
    print "L->",L
    print "C->",C
    print '-'*50
    return r

