

from fake import myinfer,_C




'''
(define M1 '((lambda (x) x) 5))
(define M2 '((lambda (x) (sub1 x)) 5))
(define M3 '((lambda (x) 1) ((lambda (x) (x x)) (lambda (x) (x x))))  )
(define M4 '((lambda (x) ((and x) #f)) #t))
'''
def gen_ast3():
    '''
    (&apply 
        (&lambda (x) (&const 1)) 
        (&apply 
            (&lambda (x) 
                (&apply (&var x) (&var x))
            ) 
            (&lambda (x)
                (&apply (&var x) (&var x))
            )
        )
    )
    '''
    e21_1 = ['&var', 'x']
    e21_2 = e21_1
    body21 = ['&apply', e21_1, e21_2]
    lam21 = ['&lambda', 'x', body21]
    lam22 = lam21
    e2 = ['&apply', lam21 , lam22 ]
    body1 = ['&const', 1]
    lam1 = ['&lambda', 'x', body1]
    return ['&apply', lam1, e2]

def gen_ast4():
    '''
    (&apply 
        (&lambda (x) (&apply (&apply (&var and) (&var x)) (&const false)))
        (&const true)
    )
    '''
    e12 = ['&var', 'x']
    e11 = ['&var', 'and']
    e1 = ['&apply', e11, e12] #(&apply (&var and) (&var x))
    e2 = ['&const', False]
    body = ['&apply', e1, e2]
    lam = ['&lambda', 'x' , body]
    con_v = ['&const', True]
    return ['&apply', lam, con_v] 



'''
(define TEST6 '((lambda (x) (and x) ) (((lambda (y) (or y)) (((lambda (z) (and z)) #f ) #t) ) #t)))
(define TEST7 '((lambda (x) (sub1 x) ) (((lambda (y) (or y) #t)) #t)))
'''
def gen_ast5():
    '''
    (define TEST5 '(lambda (x) (lambda (y) (lambda (z) (add1 z)))))
    (&lambda (x) 
        (&lambda (y) 
            (&lambda (z) 
                (&apply (&var add1) (&var z))
            )
        )
    )
    '''
    e2 = ['&var', 'z']
    e1 = ['&var', 'add1']
    bodyz = ['&apply', e1, e2]
    bodyy = ['&lambda', 'z', bodyz]
    bodyx = ['&lambda', 'y', bodyy]
    return ['&lambda', 'x', bodyx]

def gen_ast6():
    '''
    (&apply 
        (&lambda (x) (&apply (&var and) (&var x)))   e1 
        (&apply 
            (&apply 
                (&lambda (y) (&apply (&var or) (&var y))) e21_1
                (&apply 
                    (&apply 
                        (&lambda (z) (&apply (&var and) (&var z)))  a11
                        (&const false) a12
                    ) a1
                    (&const true) a2
                ) e21_2
            )   e21
            (&const true) e22
        )  e2
    )
    '''

    '''e2'''
    z2 = ['&var', 'z']
    z1 = ['&var', 'and']
    body_a11 = ['&apply', z1 , z2]
    a11 = ['&lambda', 'z', body_a11]
    a12 = ['&const', False]
    a1 = ['&apply', a11, a12] 
    a2 = ['&const', True]
    e21_2 = ['&apply', a1, a2]

    e_21_1_2 = ['&var', 'y']
    e_21_1_1 = ['&var', 'or'] # e_21_1_1 = ['&var', 'or']
    body_21_1 = ['&apply', e_21_1_1, e_21_1_2]
    e21_1 = ['&lambda', 'y', body_21_1]
    e21 = ['&apply', e21_1, e21_2]
    e22 = ['&const', True]
    e2= ['&apply', e21, e22]

    '''e1'''
    e12 = ['&var', 'x']
    e11 = ['&var', 'and']
    body1 = ['&apply', e11, e12]
    e1 = ['&lambda', 'x', body1]
    return ['&apply', e1, e2]

##############################################################
#print 'M2 out:', myinfer(_ast2, _C[:])
#print 'M4 out:', myinfer( gen_ast4(), _C[:])
#print 'M3 out:', myinfer( gen_ast3(), _C[:] ) # ((lambda (x) (x x))  !dirty
print 'T5 out:', myinfer( gen_ast5(), _C[:])
print "*-*"*30
print 'T6 out:', myinfer( gen_ast6(), _C[:])
print "*-*"*30

