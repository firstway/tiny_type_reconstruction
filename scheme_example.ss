

(let ((f +) (x 2) (y 3))  (f x y) ) 


(let ((x 0) (y 1))
    (let* ((x y) (y x))
          (list x y))) ; >>> (1 1)

(let* (  (n (+ 1 5))
         (a (+ n 10) )
      )
      (* 10 a)
  )


;
;(define var0
;  (lambda varr
;      e1 e2 ...))
;
(define mutil_app (lambda (x) 
                        (* 2 x) ; body1
                        (+ 2 x) ; body2
                    )
  )


; (begin expr1 expr2 ...) 
( begin (+ 1 1) (+ 2 2) (+ 3 3))


;
;syntax: (if test consequent alternative) 
;syntax: (if test consequent) 
;



;syntax: (cond clause1 clause2 ...) 
;returns: see below 
;libraries: (rnrs base), (rnrs)
;
;Each clause but the last must take one of the forms below.
;
;(test)
;(test expr1 expr2 ...)
;(test => expr)

(define x 8)
(cond 
  ( (cons 'a '()) => car )
  )





;syntax: (case expr0 clause1 clause2 ...) 
;
;Each clause but the last must take the form
;
;((key ...) expr1 expr2 ...)
;
;compared (using eqv?) 
(case x
  ( (1 2 3 8) (+ 1 x) (+ 2 x) )
)





